import java.util.Scanner;

public class Program1 {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите количество чисел, которые будут проанализированны:");
            int lengthArr = scanner.nextInt();
            System.out.println("Введите числа в диапазоне от -1000 до 1000:");
            
            int[] test = new int[lengthArr];
            
            for (int i = 0; i < test.length; i++)
            {
                int unit = scanner.nextInt();
                if (unit >= -1000 && unit <= 1000){
                    test[i] = unit;
                }else{
                    System.out.println("Введеное число вне диапазона");
                    break;
                }

            }
            int result = maxValueOfArray(test);
            System.out.println("Максимальное количестов раз (из последних введенных) повторяется число: " + result);

        }
        //поиск повторений, создание массива из количества повторений каждого элемента
        public static int maxValueOfArray(int[] array) {
                int[] result = new int[array.length];
                int number = 0;
                for (int i=0; i < array.length; i++){
                    for (int j=0; j < array.length; j++) {
                        if (array[i]==array[j])
                            result[i]++;
                    }
                }

                //получение элемента показывающего самое большое количество повторений
                int max = result[0];
                for (int i = 0; i < result.length; i++){
                    if (result[i] >= max) max = result[i]; //количество максимальных повторений
                }

                //получение последнего повторяющегося элемента в массиве
                for (int i = result.length-1; i >= 0 ; i--){
                    if(max == result[i]) {
                    number = i;
                    break;
                    }
                }

            /*Тест: кол-во повторений + адрес последнего повторяющегося числа
            System.out.println(max + " " + number);
             */
            
            return array[number];
        }
}
