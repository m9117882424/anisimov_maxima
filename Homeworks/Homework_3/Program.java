import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int currentNumber = scanner.nextInt();
        while (currentNumber != -1) {

            if (isEven(currentNumber) == true) {
                System.out.println(minDigit(currentNumber));

            } else {
                System.out.println(getDigitsAverage(currentNumber));
            }

            currentNumber = scanner.nextInt();
        }
    }

    public static int minDigit(int number) {

        String s = Integer.toString(number);

        int[] arr = new int[s.length()];
        for (int i = s.length() - 1; i >= 0; i--) {

            arr[i] = number % 10;

            number /= 10;
        }

        int min = arr[0];

        for (int i = 1; i < arr.length; i++) {

            min = Math.min(min, arr[i]);

        }
        return min;
    }

    public static boolean isEven(int number) {

        boolean result = false;

        if (number % 2 == 0) result = true;

        return result;
    }

    public static double getDigitsAverage(int number) {
        String s = Integer.toString(number);
        int[] arr = new int[s.length()];

        for (int i = s.length() - 1; i >= 0; i--) {
            arr[i] = number % 10;
            number /= 10;
        }

        int sum = arr[0];
        int product = arr[0];

        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
            product = product * arr[i];
        }
        return (double) sum / product;
    }
}