import java.util.Scanner;

class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
// счетчик валидных чисел
        int count = 0;

//цикл для ограничения количества вводимых чисел, сравнения с условием и подсчета количества валидных чисел
        for (int i = 0; i < 10; i++ )
        {
           int number = scanner.nextInt();
           int sum = 0;

//считаем сумму цифр введенного числа
          /* for (; number != 0; )
           {
               sum = sum + number % 10;
               number = number / 10;
           }
          */
// или так
               while (number !=0){
                    sum = sum + number % 10;
                    number = number / 10;
               }
               

           
//сравниваем с контрольным числом 18 и прибавляем счетчик валидных чисел
           if (sum < 18) count++;

        }
// результат и вывод на экран
        System.out.println("quantity of good numbers: " + count);
    }
}