import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(Arrays.toString(RandomArray(n)));
        int[] test = {5, 2, 512, 22};
        compareArrs(test, CloneArray(test));
    }

    public static int[] RandomArray(int n) {
        Random r = new Random();
        int[] random = new int[n];
        for (int i = 0; i < random.length; i++) {
            random[i] = r.nextInt();

        }
        return random;
    }

    public static int[] CloneArray(int[] arr) {
        //переворот числа и создание копии массива
        int[] newArr = Arrays.copyOf(arr, arr.length);


        for (int i = 0; i < newArr.length; i++) {
            if (newArr[i] / 100 >= 1 && newArr[i] / 100 <= 9) {
                int revers = 0;
                int oldI = newArr[i];
                while (newArr[i] != 0) {
                    revers = revers * 10 + (newArr[i] % 10);
                    newArr[i] = newArr[i] / 10;
                }
                newArr[i] = revers + oldI;
            }
        }
        return newArr;
    }

    public static void compareArrs(int[] arr1, int[] arr2) {
        int numberOfElement = 0;
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                numberOfElement = i;
                break;
            }
        }
        System.out.println(Arrays.toString(arr1));
        System.out.println(Arrays.toString(arr2));
        System.out.println("номер отличного элемента: " + numberOfElement);
    }


}
